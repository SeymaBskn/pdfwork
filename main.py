from io import StringIO

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdftypes import resolve1
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams

import PyPDF2
import sys

'''Read page number of pdfs

with open("first.pdf", "rb") as file:
    reader = PyPDF2.PdfFileReader(file)
    print(reader.numPages)
'''

'''Merge pdfs
pdfs = sys.argv[1:]

def merge_pdfs(files):
    merger = PyPDF2.PdfFileMerger()
    for file in files:
        merger.append(file)
    merger.write("merged.pdf")

merge_pdfs(pdfs)
'''

'''Merge WATERMARK to each page of input file
with open("merged.pdf", "rb") as mergedfile, open("wtr.pdf", "rb") as watermarkfile:

    merged_reader = PyPDF2.PdfFileReader(mergedfile)
    watermark_reader = PyPDF2.PdfFileReader(watermarkfile)
    watermark_page = watermark_reader.getPage(0)

    output = PyPDF2.PdfFileWriter()

    for pagenum in range(merged_reader.numPages):
        pdf_page = merged_reader.getPage(pagenum)
        pdf_page.mergePage(watermark_page)
        output.addPage(pdf_page)

    with open("watermarked.pdf", "wb") as watermarked_file:
        output.write(watermarked_file)
'''

'''Read all pdfs and get the text
output_string = StringIO()

with open("first.pdf", "rb") as file:
    parser = PDFParser(file)
    doc = PDFDocument(parser)
    resource_manager = PDFResourceManager()
    device = TextConverter(resource_manager, output_string, laparams=LAParams())
    interpreter = PDFPageInterpreter(resource_manager, device)
    
    for page in PDFPage.create_pages(doc):
        interpreter.process_page(page)

print(output_string.getvalue())
'''
